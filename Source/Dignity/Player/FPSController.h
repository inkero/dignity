// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "FPSController.generated.h"

/**
 * 
 */
UCLASS()
class DIGNITY_API AFPSController : public APlayerController
{
	GENERATED_BODY()

public:
	AActor* FindRandomRespawnPoint();

	
	
};
