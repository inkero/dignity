// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "DignityGameModeBase.h"
#include "Player/FPSCharacter.h"
#include "DeathmatchGameMode.generated.h"

/**
 * 
 */
UCLASS()
class DIGNITY_API ADeathmatchGameMode : public ADignityGameModeBase
{
	GENERATED_BODY()
	
public:
	ADeathmatchGameMode();

	// Called on play
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
protected:
	bool bDo;
	void MyDoOnce(AFPSCharacter* OtherPlayer);
	
};
