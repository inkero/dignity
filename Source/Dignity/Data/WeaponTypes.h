// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WeaponTypes.generated.h"

UENUM(BlueprintType)
enum class EWeaponTypes : uint8 {
	W_Main			UMETA(DisplayName = "Main"),
	W_Secondary		UMETA(DisplayName = "Secondary"),
	W_Special		UMETA(DisplayName = "Special"),
	W_Grenade		UMETA(DisplayName = "Grenade"),
	W_Melee			UMETA(DisplayName = "Melee"),
};

