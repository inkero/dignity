// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "Multiplayer/RespawnPoint.h"
#include "Engine/World.h"
#include "DignityGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class DIGNITY_API ADignityGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category = "Respawning")
	AActor* FindRandomRespawnPoint();
	
	
};
