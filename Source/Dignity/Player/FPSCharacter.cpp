﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSCharacter.h"
#include "UnrealNetwork.h"
//#include "Engine.h"


// Sets default values
AFPSCharacter::AFPSCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Weapon manager
	{
		WeaponManager = CreateDefaultSubobject<UWeaponManager>(TEXT("WeaponManager"));
		WeaponManager->SetIsReplicated(true);
	}

	// Camera settings
	{
		Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
		Camera->SetupAttachment(GetCapsuleComponent());
		Camera->SetRelativeLocation(FVector(25.0f, 0.0f, 75.0f));
		Camera->bUsePawnControlRotation = true;
		Camera->FieldOfView = 115.0;

	}

	// 1st person mesh settings
	{
		SwayOffset = CreateDefaultSubobject<USceneComponent>(TEXT("SwayOffset"));
		SwayOffset->SetupAttachment(Camera);

		Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh1P"));
		Mesh1P->SetupAttachment(SwayOffset);
		Mesh1P->bOnlyOwnerSee = true;
		Mesh1P->SetCastShadow(false);
	}

	// 3rd person mesh settings
	{
		Mesh3P = GetMesh();
		Mesh3P->bOwnerNoSee = true;
	}

	// General character settings
	{

		StartingHealth = 100;
		Health = StartingHealth;

		CameraFOV = Camera->FieldOfView;
		SprintingSpeed = 900;

		Aiming = false;
		Sprinting = false;
		IsFirePressed = false;
		CanFire = true;
		landed = true;
	}

	/*
	CurrentWeapon3P = NewObject<AWeapon>(this, StartingWeaponBP, FName("CurrentWeapon3P"));
	CurrentWeapon3P->AttachToComponent(Mesh3P, FAttachmentTransformRules::KeepRelativeTransform, "Weapon_Attach");
	CurrentWeapon3P->SetOwner(this);
	CurrentWeapon3P->Mesh->SetCastShadow(true);
	CurrentWeapon3P->Mesh->bOnlyOwnerSee = false;
	CurrentWeapon3P->Mesh->bOwnerNoSee = true;
	*/
}

// Called when the game starts or when spawned
void AFPSCharacter::BeginPlay()
{
	Super::BeginPlay();


	WeaponManager->CreateWeapon(StartingWeaponBP);

	// Remember the initial position of SwayOffset for later use
	SwayOffsetDefaultLocation = SwayOffset->RelativeLocation;
	SwayOffsetReturnLocation = SwayOffsetDefaultLocation;

	// Remember the initial rotation of SwayOffset for later use
	SwayOffsetDefaultRotation = SwayOffset->RelativeRotation;
	SwayOffsetReturnRotation = SwayOffsetDefaultRotation;



}

// Called every frame
void AFPSCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	WeaponSway();

	if (this->HasAuthority()) {
		SpineRotation();

		// Allow these functions only if we have a weapon spawned
		if (CurrentWeapon1P) {
			ClientFire();
			Zoom();
			Recoil();
			ClientPlayCameraShake();
		}

	}

}

// Called to bind functionality to input
void AFPSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAxis("MousePitch", this, &AFPSCharacter::MousePitch);
	InputComponent->BindAxis("MouseYaw", this, &AFPSCharacter::MouseYaw);

	InputComponent->BindAxis("MoveForward", this, &AFPSCharacter::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AFPSCharacter::MoveRight);

	InputComponent->BindAction("Fire", IE_Pressed, this, &AFPSCharacter::FirePressed);
	InputComponent->BindAction("Fire", IE_Released, this, &AFPSCharacter::FireReleased);
	InputComponent->BindAction("AimDownSights", IE_Pressed, this, &AFPSCharacter::AimDownSights);

	InputComponent->BindAction("Sprint", IE_Pressed, this, &AFPSCharacter::SprintPressed);
	InputComponent->BindAction("Sprint", IE_Released, this, &AFPSCharacter::SprintReleased);

	InputComponent->BindAction("Jump", IE_Pressed, this, &AFPSCharacter::DoJump);

}

void AFPSCharacter::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AFPSCharacter, ControlPitch);

}

// Look up / down
void AFPSCharacter::MousePitch(float Axis)
{
	AddControllerPitchInput(Axis);
	MousePitchAxis = Axis;
}

// Look right / left
void AFPSCharacter::MouseYaw(float Axis)
{
	AddControllerYawInput(Axis);
	MouseYawAxis = Axis;
}

// Forward / backward movement
void AFPSCharacter::MoveForward(float Axis)
{
	FVector ForwardVector = GetActorForwardVector();
	AddMovementInput(ForwardVector, Axis);
}

// Right / left movement
void AFPSCharacter::MoveRight(float Axis)
{
	FVector RightVector = GetActorRightVector();
	AddMovementInput(RightVector, Axis);

	// Weapon strafe movement
	{
		float StrafeMovementAmount = -1.5f;
		float InterpSpeed = 1.1f;

		// Movement left / right
		if (!Aiming) {
			FVector StrafeVector = FVector(0.0f, Axis * StrafeMovementAmount, 0.0f);

			FVector NewLocation = FMath::VInterpTo(SwayOffset->RelativeLocation, SwayOffsetReturnLocation + StrafeVector, GetWorld()->GetDeltaSeconds(), InterpSpeed);
			SwayOffset->SetRelativeLocation(NewLocation);
		}

		// Tilting
		FRotator StrafeRotation = FRotator(Axis * 30.0f, 0.0f, 0.0f);

		FRotator NewRotation = FMath::RInterpTo(SwayOffset->RelativeRotation, SwayOffsetReturnRotation + StrafeRotation, GetWorld()->GetDeltaSeconds(), InterpSpeed);
		SwayOffset->SetRelativeRotation(NewRotation);

	}
	
}

// Weapon X/Y movement when walking or strafing
void AFPSCharacter::WeaponMovement(float Axis) {

}

// Setting player new max speed and replicating it to everyone
void AFPSCharacter::SetMaxWalkSpeed(float NewSpeed) {
	GetCharacterMovement()->MaxWalkSpeed = NewSpeed;
}

// Client/Server settings for previous function
void AFPSCharacter::ClientSetMaxWalkSpeed_Implementation(float NewSpeed) {
	SetMaxWalkSpeed(NewSpeed);
	ServerSetMaxWalkSpeed(NewSpeed);
}
bool AFPSCharacter::ClientSetMaxWalkSpeed_Validate(float NewSpeed) {
	return true;
}
void AFPSCharacter::ServerSetMaxWalkSpeed_Implementation(float NewSpeed) {
	MulticastSetMaxWalkSpeed(NewSpeed);
}
bool AFPSCharacter::ServerSetMaxWalkSpeed_Validate(float NewSpeed) {
	return true;
}
void AFPSCharacter::MulticastSetMaxWalkSpeed_Implementation(float NewSpeed) {
	SetMaxWalkSpeed(NewSpeed);
}
bool AFPSCharacter::MulticastSetMaxWalkSpeed_Validate(float NewSpeed) {
	return true;
}

// Spawns a particular weapon for the player
void AFPSCharacter::SpawnWeapon(TSubclassOf<AWeapon> WeaponBP)
{
	/*
	// 1st person weapon actor spawning & attaching
	AWeapon* SpawnedWeapon1P = GetWorld()->SpawnActor<AWeapon>(WeaponBP);
	if (SpawnedWeapon1P) {
		SpawnedWeapon1P->AttachToComponent(Mesh1P, FAttachmentTransformRules::KeepRelativeTransform);
		CurrentWeapon1P = SpawnedWeapon1P;
		CurrentWeapon1P->SetOwner(this);
	}


	// 3rd person weapon actor spawning & attaching
	AWeapon* SpawnedWeapon3P = GetWorld()->SpawnActor<AWeapon>(WeaponBP);
	if (SpawnedWeapon3P) {
		SpawnedWeapon3P->AttachToComponent(Mesh3P, FAttachmentTransformRules::KeepRelativeTransform, "Weapon_Attach");
		CurrentWeapon3P = SpawnedWeapon3P;
		CurrentWeapon3P->SetOwner(this);
		CurrentWeapon3P->Mesh->SetCastShadow(true);
		CurrentWeapon3P->Mesh->bOnlyOwnerSee = false;
		CurrentWeapon3P->Mesh->bOwnerNoSee = true;
	}
	*/
}

// Save IsFirePressed bool for usage in Fire() since its used on tick
void AFPSCharacter::FirePressed()
{
	IsFirePressed = true;
}

void AFPSCharacter::FireReleased()
{
	IsFirePressed = false;
}

void AFPSCharacter::ToggleCanFire() {
	CanFire = true;
}

void AFPSCharacter::ClientFire_Implementation()
{
	if (CurrentWeapon1P) {
		if (IsFirePressed) {
			if (CanFire) {
				CanFire = false;
				GetWorldTimerManager().SetTimer(UnusedHandle, this, &AFPSCharacter::ToggleCanFire, 1 / CurrentWeapon1P->FireRate, false);
				FVector Start = CurrentWeapon1P->MuzzlePoint->GetComponentLocation();
				FVector ForwardVector = CurrentWeapon1P->MuzzlePoint->GetForwardVector();
				FVector End = Start + (ForwardVector * 9999.0f);
			
				FHitResult OutHit;
				FCollisionQueryParams CollisionParams;
				CollisionParams.AddIgnoredActor(this);
				
				//DrawDebugLine(GetWorld(), Start, End, FColor::Red, false, 10, 0, 1);

				bool IsHit = GetWorld()->LineTraceSingleByObjectType(OutHit, Start, End, FCollisionObjectQueryParams(ECC_TO_BITFIELD(ECC_WorldStatic) | /*ECC_TO_BITFIELD(ECC_WorldDynamic) |*/ ECC_TO_BITFIELD(ECC_PhysicsBody) | ECC_TO_BITFIELD(ECC_Pawn)), CollisionParams);

				if (IsHit) {
					if (OutHit.bBlockingHit) {

						AFPSCharacter* OtherPlayer = Cast<AFPSCharacter>(OutHit.GetActor());

						if (OtherPlayer != nullptr) {

							// Show hit marker only on our character
							if (IsLocallyControlled()) {
								UGameplayStatics::PlaySound2D(GetWorld(), HitMarkerSound, 5.0f, 1.0f, 0.00f);
								if (HitMarkerClass != nullptr) {
									HitMarkerWidget = CreateWidget<UUserWidget>(GetWorld(), HitMarkerClass);
									if (HitMarkerWidget != nullptr) {
										HitMarkerWidget->AddToViewport();
									}
								}
							}

							RegisterHitToServer(OutHit, OtherPlayer);

						}
						else {
							UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), CurrentWeapon1P->BulletHitParticle, OutHit.ImpactPoint);
							UDecalComponent* SpawnedDecal = UGameplayStatics::SpawnDecalAttached(CurrentWeapon1P->BulletHoleMaterial, FVector(2.0f, 7.0f, 7.0f), OutHit.GetComponent(), NAME_None, OutHit.ImpactPoint, OutHit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 20.0f);
							SpawnedDecal->SetFadeScreenSize(0.001f);

							//UE_LOG(LogTemp, Warning, TEXT("Decal is spawned"));

							if (OutHit.GetActor()) {
								if (OutHit.GetActor()->IsRootComponentMovable()) {
									if (OutHit.GetActor()->GetRootComponent()->IsSimulatingPhysics()){
										UStaticMeshComponent* StaticMeshRootComp = Cast<UStaticMeshComponent>(OutHit.GetActor()->GetRootComponent());
										if (StaticMeshRootComp) {
											StaticMeshRootComp->AddForce(Camera->GetForwardVector() * 12000.0f * StaticMeshRootComp->GetMass());
										}
										USkeletalMeshComponent* SkeletalMeshRootComp = Cast<USkeletalMeshComponent>(OutHit.GetActor()->GetRootComponent());
										if (SkeletalMeshRootComp) {
											SkeletalMeshRootComp->AddForce(Camera->GetForwardVector() * 12000.0f * SkeletalMeshRootComp->GetMass());
										}
									}
								}
							}
							
							

							

							RegisterHitToServer(OutHit);
						}
					}
				}


				// Weapon recoil
				SwayOffset->AddRelativeLocation(FVector(-0.8f, 0.0f, 0.0f));

				// Camera recoil
				VerticalRecoil = -0.65f;
				HorizontalRecoil = FMath::FRandRange(0.05f, 0.3f);

				// Play fire sound
				UGameplayStatics::PlaySound2D(GetWorld(), FireSoundClose, 1.0f, 1.0f, 0.0f);
				//UGameplayStatics::PlaySound2D(GetWorld(), FireSoundDistant, 1.0f, 1.0f, 0.0f);

				// Play particle muzzle effect
				UParticleSystem* Particle = CurrentWeapon1P->MuzzleParticleSystem;

				UGameplayStatics::SpawnEmitterAttached(Particle, CurrentWeapon1P->MuzzlePoint, FName("MuzzlePointParticleSystem"), CurrentWeapon1P->MuzzlePoint->GetComponentLocation(), CurrentWeapon1P->MuzzlePoint->GetComponentRotation(), CurrentWeapon1P->MuzzlePoint->GetComponentScale(), EAttachLocation::KeepWorldPosition);
			}
		}
	}
}

bool AFPSCharacter::ClientFire_Validate()
{
	return true;
}

void AFPSCharacter::RegisterHitToServer_Implementation(FHitResult OutHit, AFPSCharacter* OtherPlayer)
{
	//OLD
	//MulticastFire(Start, End);

	//NEW
	MulticastHitToEveryPlayer(OutHit, OtherPlayer);
}

bool AFPSCharacter::RegisterHitToServer_Validate(FHitResult OutHit, AFPSCharacter* OtherPlayer)
{
	return true;
}

void AFPSCharacter::MulticastHitToEveryPlayer_Implementation(FHitResult OutHit, AFPSCharacter* OtherPlayer) {
	
	if (CurrentWeapon1P) {

		if (OtherPlayer != nullptr){
			OtherPlayer->GetDamage(26.0f);
		}
		else {
			if (!IsLocallyControlled()) {
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), CurrentWeapon1P->BulletHitParticle, OutHit.ImpactPoint);
				UDecalComponent* SpawnedDecal = UGameplayStatics::SpawnDecalAttached(CurrentWeapon1P->BulletHoleMaterial, FVector(2.0f, 7.0f, 7.0f), OutHit.GetComponent(), NAME_None, OutHit.ImpactPoint, OutHit.ImpactNormal.Rotation(), EAttachLocation::KeepRelativeOffset, 20.0f);
				SpawnedDecal->SetFadeScreenSize(0.001f);

				
				if (OutHit.GetActor()) {
					if (OutHit.GetActor()->IsRootComponentMovable()) {
						UStaticMeshComponent* MeshRootComp = Cast<UStaticMeshComponent>(OutHit.GetActor()->GetRootComponent());
						MeshRootComp->AddForce(Camera->GetForwardVector() * 12000.0f * MeshRootComp->GetMass());
					}
				}
				
			}
		}

		if (!IsLocallyControlled()) {
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), FireSoundDistant, Camera->GetComponentLocation(), 1.0f, 1.0f, 0.0f, CurrentWeapon1P->ShotAttenuation);

			// Play particle muzzle effect for other players (3rd person weapon model)
			UParticleSystem* Particle = CurrentWeapon1P->MuzzleParticleSystem;
			UGameplayStatics::SpawnEmitterAttached(Particle, CurrentWeapon3P->MuzzlePoint, FName("MuzzlePointParticleSystem"), CurrentWeapon3P->MuzzlePoint->GetComponentLocation(), CurrentWeapon3P->MuzzlePoint->GetComponentRotation(), CurrentWeapon3P->MuzzlePoint->GetComponentScale(), EAttachLocation::KeepWorldPosition);
		}
	}

}

bool AFPSCharacter::MulticastHitToEveryPlayer_Validate(FHitResult OutHit, AFPSCharacter* OtherPlayer)
{
	return true;
}

void AFPSCharacter::Recoil() {
	float ApplyPitch;
	VerticalRecoil = FMath::FInterpTo(VerticalRecoil, 0, GetWorld()->GetDeltaSeconds(), 10.0f);
	VerticalRecoilRecovery = FMath::FInterpTo(VerticalRecoilRecovery, -VerticalRecoil, GetWorld()->GetDeltaSeconds(), 20.0f);
	ApplyPitch = VerticalRecoil + VerticalRecoilRecovery;
	AddControllerPitchInput(ApplyPitch);

	float ApplyYaw;
	HorizontalRecoil = FMath::FInterpTo(HorizontalRecoil, 0, GetWorld()->GetDeltaSeconds(), 10.0f);
	HorizontalRecoilRecovery = FMath::FInterpTo(HorizontalRecoilRecovery, -HorizontalRecoil, GetWorld()->GetDeltaSeconds(), 20.0f);
	ApplyYaw = HorizontalRecoil + HorizontalRecoilRecovery;
	AddControllerYawInput(ApplyYaw);
}

// Aim down the sights by changing Mesh1P location
void AFPSCharacter::AimDownSights()
{

	FVector CameraLocation = Camera->RelativeLocation;
	FVector SightOffsetLocation = CurrentWeapon1P->SightOffset->RelativeLocation;
	FVector test3 = CameraLocation + SightOffsetLocation;

	if (!Aiming) {
		SwayOffsetReturnLocation = SwayOffsetReturnLocation + FVector(-0.75f, -3.45f, -0.3f);
		ClientSetMaxWalkSpeed(400);
		Aiming = true;
	}
	else {
		SwayOffsetReturnLocation = SwayOffsetDefaultLocation;
		ClientSetMaxWalkSpeed(550);
		Aiming = false;
	}

}

// Zoom is used in tick for the interpolation to work
void AFPSCharacter::Zoom()
{
	if (Aiming) {
		float NewFOV = FMath::FInterpTo(Camera->FieldOfView, 100.0f, GetWorld()->GetDeltaSeconds(), 30);
		Camera->SetFieldOfView(NewFOV);

	}
	else {
		float NewFOV = FMath::FInterpTo(Camera->FieldOfView, CameraFOV, GetWorld()->GetDeltaSeconds(), 30);
		Camera->SetFieldOfView(NewFOV);

	}
}

// Character speed change on sprint pressed
void AFPSCharacter::SprintPressed()
{
	if (Aiming) {
		AimDownSights();
	}

	Sprinting = true;

	ClientSetMaxWalkSpeed(SprintingSpeed);
	//GetCharacterMovement()->Velocity.Size()
}

// Character speed change on sprint released
void AFPSCharacter::SprintReleased()
{
	if (Aiming) {
		ClientSetMaxWalkSpeed(400);
	}
	else {
		ClientSetMaxWalkSpeed(550);
	}

	Sprinting = false;

}

void AFPSCharacter::DoJump()
{
	if (landed) {
		UGameplayStatics::PlaySound2D(GetWorld(), JumpSound, 1.0f, 1.0f, 0.0f);
		Jump();
	}

	landed = false;
}


void AFPSCharacter::Landed(const FHitResult& Hit) {
	Super::Landed(Hit);

	if (IsLocallyControlled())
	{
		UGameplayStatics::PlaySound2D(GetWorld(), LandSound, 1.0f, 1.0f, 0.0f);
	}
	landed = true;
}

void AFPSCharacter::ClientPlayCameraShake_Implementation() {
	if (GetCharacterMovement()->Velocity.Size() > 700) {
		GetWorld()->GetFirstPlayerController()->ClientPlayCameraShake(runCameraShake);
	}
	else if (GetCharacterMovement()->Velocity.Size() > 0) {
		GetWorld()->GetFirstPlayerController()->ClientPlayCameraShake(walkCameraShake);
	}
}

bool AFPSCharacter::ClientPlayCameraShake_Validate() {
	return true;
}

// Weapon sway and tilting
void AFPSCharacter::WeaponSway()
{

	float SwaySmoothing = 8.0f;

	float SwayAmount = 0.2f;
	float MaxSwayAmount = 1.5f;

	if (Aiming) {
		MaxSwayAmount = 0.4f;
	}

	// Weapon sway
	{
		float YawSway = FMath::Clamp(MouseYawAxis * SwayAmount * -1.0f, -MaxSwayAmount, MaxSwayAmount);
		float PitchSway = FMath::Clamp(MousePitchAxis * SwayAmount, -MaxSwayAmount, MaxSwayAmount);

		FVector SwayVector = FVector(0.0f, YawSway, PitchSway);

		FVector NewLocation = FMath::VInterpTo(SwayOffset->RelativeLocation, SwayOffsetReturnLocation + SwayVector, GetWorld()->GetDeltaSeconds(), SwaySmoothing);
		SwayOffset->SetRelativeLocation(NewLocation);
	}

	float TiltAmount = 2.8f;
	float MaxTiltAmount = 3.5f;

	// Weapon tilt
	{
		float YawTilt = FMath::Clamp(MouseYawAxis * TiltAmount, -MaxTiltAmount, MaxTiltAmount);

		FRotator TiltRotator = FRotator(YawTilt, 0.0f, 0.0f);

		FRotator NewRotation = FMath::RInterpTo(SwayOffset->RelativeRotation, SwayOffsetReturnRotation + TiltRotator, GetWorld()->GetDeltaSeconds(), SwaySmoothing);
		SwayOffset->SetRelativeRotation(NewRotation);
	}
}

void AFPSCharacter::SpineRotation()
{
	float SelectFloat = 0;
	if (GetControlRotation().Pitch > 180.0f) {
		SelectFloat = 360.0f - GetControlRotation().Pitch;
	}
	else {
		SelectFloat = GetControlRotation().Pitch * -1.0f;
	}

	FRotator NewControlPitch = FRotator(0.0f, 0.0f, SelectFloat / 3.0f);
	ControlPitch = NewControlPitch;
}

void AFPSCharacter::GetDamage(float Damage)
{
	Health = Health - Damage;

	
	if (Health <= 0) {
		//GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Green, FString::Printf(TEXT("Player dead!")));
		//TeleportTo(FindRandomRespawnPoint()->GetActorLocation(), FindRandomRespawnPoint()->GetActorRotation());
		//Health = StartingHealth;

		

		if (KillMarkerClass != nullptr) {
			KillMarkerWidget = CreateWidget<UUserWidget>(GetWorld(), KillMarkerClass);
			if (KillMarkerWidget != nullptr) {
				//KillMarkerWidget->AddToViewport();
			}
		}

		CurrentWeapon1P->Destroy();
		CurrentWeapon3P->Destroy();
		Destroy();
	}
	
}
