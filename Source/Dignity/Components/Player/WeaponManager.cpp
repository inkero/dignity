// Fill out your copyright notice in the Description page of Project Settings.

#include "WeaponManager.h"
#include "../../Player/FPSCharacter.h"
#include "../../Weapons/Weapon.h"
#include "UnrealNetwork.h"

// Sets default values for this component's properties
UWeaponManager::UWeaponManager()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

void UWeaponManager::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	
	//DOREPLIFETIME_CONDITION(UWeaponManager, MainWeapon, COND_OwnerOnly); // Causes crash? Can't find MainWeapon when loading second player
	//DOREPLIFETIME_CONDITION(UWeaponManager, SecondaryWeapon, COND_OwnerOnly);
	//DOREPLIFETIME_CONDITION(UWeaponManager, SpecialWeapon, COND_OwnerOnly);
	//DOREPLIFETIME_CONDITION(UWeaponManager, Grenade, COND_OwnerOnly);
	//DOREPLIFETIME_CONDITION(UWeaponManager, Melee, COND_OwnerOnly);
	
}


// Called when the game starts
void UWeaponManager::BeginPlay()
{
	Super::BeginPlay();

	Character = Cast<AFPSCharacter>(GetOwner());
	
	
}


// Called every frame
void UWeaponManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UWeaponManager::CreateWeapon(TSubclassOf<AWeapon> Weapon) {
	FActorSpawnParameters Params;
	Params.Owner = Character->GetOwner();
	Params.Instigator = Character;
	Params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	AWeapon* SpawnedWeapon1P = GetWorld()->SpawnActor<AWeapon>(Weapon, Params);
	AWeapon* SpawnedWeapon3P = GetWorld()->SpawnActor<AWeapon>(Weapon, Params);

	switch (SpawnedWeapon1P->WeaponType) {
		case EWeaponTypes::W_Main: {
			MainWeapon = SpawnedWeapon1P;
		}
		case EWeaponTypes::W_Secondary: {
			SecondaryWeapon = SpawnedWeapon1P;
		}
		case EWeaponTypes::W_Special: {
			SpecialWeapon = SpawnedWeapon1P;
		}
		case EWeaponTypes::W_Grenade: {
			Grenade = SpawnedWeapon1P;
		}
		case EWeaponTypes::W_Melee: {
			Melee = SpawnedWeapon1P;
		}
	}

	if (SpawnedWeapon1P) {
		SpawnedWeapon1P->AttachToComponent(Character->Mesh1P, FAttachmentTransformRules::KeepRelativeTransform);
		Character->CurrentWeapon1P = SpawnedWeapon1P;
		Character->CurrentWeapon1P->SetOwner(Character); // Replace currentweapon with spawnedweapon, so no need for this? 
	}


	// 3rd person weapon actor spawning & attaching
	if (SpawnedWeapon3P) {
		SpawnedWeapon3P->AttachToComponent(Character->Mesh3P, FAttachmentTransformRules::KeepRelativeTransform, "Weapon_Attach");
		Character->CurrentWeapon3P = SpawnedWeapon3P;
		Character->CurrentWeapon3P->SetOwner(Character); // Replace currentweapon with spawnedweapon, so no need for this? 
		Character->CurrentWeapon3P->Mesh->SetCastShadow(true);
		Character->CurrentWeapon3P->Mesh->bOnlyOwnerSee = false;
		Character->CurrentWeapon3P->Mesh->bOwnerNoSee = true;
	}
}   

void UWeaponManager::SelectWeapon() {

}

