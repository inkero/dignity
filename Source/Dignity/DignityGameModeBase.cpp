// Fill out your copyright notice in the Description page of Project Settings.

#include "DignityGameModeBase.h"

AActor* ADignityGameModeBase::FindRandomRespawnPoint() {

	TSubclassOf<ARespawnPoint> RespawnPointClass;
	RespawnPointClass = ARespawnPoint::StaticClass();
	TArray<AActor*> FoundRespawnPoints;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), RespawnPointClass, FoundRespawnPoints);

	AActor* RandomRespawnPoint = FoundRespawnPoints[FMath::FRandRange(0, FoundRespawnPoints.Num() - 1)];

	return RandomRespawnPoint;
}


