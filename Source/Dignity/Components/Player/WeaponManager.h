// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "WeaponManager.generated.h"

class AWeapon;
class AFPSCharacter;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DIGNITY_API UWeaponManager : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UWeaponManager();

	AFPSCharacter* Character;

	UPROPERTY(BlueprintReadOnly, Replicated)
	AWeapon* MainWeapon;

	UPROPERTY(BlueprintReadOnly, Replicated)
	AWeapon* SecondaryWeapon;

	UPROPERTY(BlueprintReadOnly, Replicated)
	AWeapon* SpecialWeapon;

	UPROPERTY(BlueprintReadOnly, Replicated)
	AWeapon* Grenade;

	UPROPERTY(BlueprintReadOnly, Replicated)
	AWeapon* Melee;

	void CreateWeapon(TSubclassOf<AWeapon> Weapon);
	void SelectWeapon();

	// We set the replicated variables here
	virtual void GetLifetimeReplicatedProps(class TArray<class FLifetimeProperty, class FDefaultAllocator> &) const override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
	
};
