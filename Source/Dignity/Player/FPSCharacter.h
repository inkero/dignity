// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/HUD.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/SceneComponent.h"
#include "Components/DecalComponent.h"
#include "UserWidget.h"
#include "../Components/Player/WeaponManager.h"
#include "../Weapons/Weapon.h"
#include "TimerManager.h"
#include "Kismet/GameplayStatics.h"

#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/UMGStyle.h"
#include "Runtime/UMG/Public/Slate/SObjectWidget.h"
#include "Runtime/UMG/Public/IUMGModule.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"

#include "FPSCharacter.generated.h"


UCLASS()
class DIGNITY_API AFPSCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AFPSCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// We set the replicated variables here
	virtual void GetLifetimeReplicatedProps(class TArray<class FLifetimeProperty, class FDefaultAllocator> &) const override;

	UPROPERTY(EditDefaultsOnly, DisplayName = "Starting Health")
	float StartingHealth;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, DisplayName = "Health")
	float Health;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, DisplayName = "Aiming")
	bool Aiming;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, DisplayName = "Sprinting")
	bool Sprinting;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, DisplayName = "Sprinting Speed")
	float SprintingSpeed;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class UUserWidget> HitMarkerClass;

	UPROPERTY()
	class UUserWidget* HitMarkerWidget;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class UUserWidget> KillMarkerClass;

	UPROPERTY()
	class UUserWidget* KillMarkerWidget;

	// Starting weapon blueprint
	UPROPERTY(EditDefaultsOnly, Category = "Spawning", DisplayName = "Starting Weapon")
	TSubclassOf<AWeapon> StartingWeaponBP;

	UPROPERTY(EditDefaultsOnly, Category = "Spawning", DisplayName = "First Weapon")
	AWeapon* FirstWeaponBP;

	UPROPERTY(EditDefaultsOnly, Category = "Sound", DisplayName = "Fire Sound Close")
	USoundBase* FireSoundClose;

	UPROPERTY(EditDefaultsOnly, Category = "Sound", DisplayName = "Fire Sound Distant")
	USoundBase* FireSoundDistant;

	UPROPERTY(EditDefaultsOnly, Category = "Sound", DisplayName = "Hit Marker Sound")
	USoundBase* HitMarkerSound;

	UPROPERTY(EditDefaultsOnly, Category = "Sound", DisplayName = "Jump Sound")
	USoundBase* JumpSound;

	UPROPERTY(EditDefaultsOnly, Category = "Sound", DisplayName = "Land Sound")
	USoundBase* LandSound;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
	TSubclassOf<UCameraShake> walkCameraShake;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
	TSubclassOf<UCameraShake> runCameraShake;

	// 1st person mesh, visible only for player
	UPROPERTY(VisibleAnywhere)
	USkeletalMeshComponent* Mesh1P;

	// 3rd person mesh, visible only to other players
	UPROPERTY(VisibleAnywhere)
	USkeletalMeshComponent* Mesh3P;

	UPROPERTY(BlueprintReadOnly)
	AWeapon* CurrentWeapon1P;

	UPROPERTY(BlueprintReadOnly)
	AWeapon* CurrentWeapon3P;

	bool landed;



	// Replicated rotator to control the Mesh3P spine rotation
	UPROPERTY(BlueprintReadOnly, Replicated)
	FRotator ControlPitch;

	UFUNCTION()
	void GetDamage(float Damage);

protected:
	//UWeaponManager* WeaponManager;

	float MousePitchAxis;
	float MouseYawAxis;

	bool Strafing;

	UPROPERTY(VisibleAnywhere)
	UWeaponManager* WeaponManager;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UCameraComponent* Camera;

	// Offset to give correct axis position for weapon sway & tilting
	UPROPERTY(VisibleAnywhere)
	USceneComponent* SwayOffset;

	// The location & rotation for the SwayOffset to return to after ADS
	FVector SwayOffsetDefaultLocation;
	FRotator SwayOffsetDefaultRotation;

	// The return location & rotation for SwayOffset after each recoil kick etc.
	FVector SwayOffsetReturnLocation;
	FRotator SwayOffsetReturnRotation;

	FTimerHandle UnusedHandle;
	bool IsFirePressed;
	bool CanFire;
	float CameraFOV;


	float VerticalRecoil;			 //Stores current negative (upwards pitch rotation) recoil
	float HorizontalRecoil;
	float VerticalRecoilRecovery;    //Stores current positive (downwards pitch rotation) recoil recovery
	float HorizontalRecoilRecovery;





	void MousePitch(float Axis);
	void MouseYaw(float Axis);

	void MoveForward(float Axis);
	void MoveRight(float Axis);

	void WeaponMovement(float Axis);

	void SetMaxWalkSpeed(float NewSpeed);

	UFUNCTION(Reliable, Client, WithValidation)
	void ClientSetMaxWalkSpeed(float NewSpeed);
	void ClientSetMaxWalkSpeed_Implementation(float NewSpeed);
	bool ClientSetMaxWalkSpeed_Validate(float NewSpeed);

	UFUNCTION(Reliable, Server, WithValidation)
	void ServerSetMaxWalkSpeed(float NewSpeed);
	void ServerSetMaxWalkSpeed_Implementation(float NewSpeed);
	bool ServerSetMaxWalkSpeed_Validate(float NewSpeed);

	UFUNCTION(Reliable, NetMulticast, WithValidation)
	void MulticastSetMaxWalkSpeed(float NewSpeed);
	void MulticastSetMaxWalkSpeed_Implementation(float NewSpeed);
	bool MulticastSetMaxWalkSpeed_Validate(float NewSpeed);

	void SpawnWeapon(TSubclassOf<AWeapon> WeaponBP);

	void FirePressed();
	void FireReleased();

	void ToggleCanFire();

	UFUNCTION(Reliable, Client, WithValidation)
	void ClientFire();
	void ClientFire_Implementation();
	bool ClientFire_Validate();

	UFUNCTION(Reliable, Server, WithValidation)
	void RegisterHitToServer(FHitResult OutHit, AFPSCharacter* OtherPlayer = nullptr);
	void RegisterHitToServer_Implementation(FHitResult OutHit, AFPSCharacter* OtherPlayer = nullptr);
	bool RegisterHitToServer_Validate(FHitResult OutHit, AFPSCharacter* OtherPlayer = nullptr);

	UFUNCTION(Reliable, NetMulticast, WithValidation)
	void MulticastHitToEveryPlayer(FHitResult OutHit, AFPSCharacter* OtherPlayer = nullptr);
	void MulticastHitToEveryPlayer_Implementation(FHitResult OutHit, AFPSCharacter* OtherPlayer = nullptr);
	bool MulticastHitToEveryPlayer_Validate(FHitResult OutHit, AFPSCharacter* OtherPlayer = nullptr);

	void Recoil();
	void AimDownSights();
	void Zoom();
	void SprintPressed();
	void SprintReleased();
	void DoJump();

	UFUNCTION(Reliable, Client, WithValidation)
	void ClientPlayCameraShake();
	void ClientPlayCameraShake_Implementation();
	bool ClientPlayCameraShake_Validate();

	virtual void Landed(const FHitResult& Hit) override;

	void WeaponSway();
	void SpineRotation();

};
